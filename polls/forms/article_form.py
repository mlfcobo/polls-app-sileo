from sileo.forms import ModelForm
from polls.models import Article


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ('headline',)
