from sileo.resource import Resource
from sileo.registration import register
from sileo.fields import ResourceTypeConvert, ResourceMethodField
from polls.models import Question
from polls.forms.question_form import QuestionForm


class QuestionResource(Resource):
    query_set = Question.objects.all()
    fields = [
        'question_text',
        ResourceTypeConvert('pub_date', str),
        ResourceMethodField(
            'text_pub_date',
            method_name='get_text_pub_date_field')
        ]

    allowed_methods = ['get_pk', 'filter', 'create', 'update', 'delete']
    filter_fields = ('question_text__icontains',)
    form_class = QuestionForm
    update_filter_fields = ('question_text',)
    delete_filter_fields = ('question_text',)

    def get_text_pub_date_field(self, prop, obj, request):
        return '%s %s' % (obj.question_text, obj.pub_date)


register('polls', 'question', QuestionResource)
