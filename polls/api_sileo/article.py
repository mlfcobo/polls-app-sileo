from sileo.resource import Resource
from sileo.registration import register
from polls.models import Article, Publication
from polls.forms.article_form import ArticleForm
from django.core import serializers
from django.core.exceptions import PermissionDenied


class ArticleResource(Resource):
    query_set = Article.objects.all()
    fields = [
        'headline',
        ]

    allowed_methods = ['create', 'get_pk', 'filter', 'update', 'delete']
    form_class = ArticleForm
    delete_filter_fields = ('pk',)

    def __init__(self, *args, **kwargs):
        # initialize query_set before accessing it
        super(ArticleResource, self).__init__(*args, **kwargs)
        self.size_per_request = self.query_set.count()

    def create(self):
        data = self.request.POST
        headline = data.get('headline')

        # add validation here
        # create article
        article = Article.objects.create(headline=headline)
        article.save()

        # add to all publications
        list_of_publication = Publication.objects.all()
        article.publications.add(*list_of_publication)

        return {
            'status': 200,
            'msg': 'creating user custom create function',
            'object': headline,
        }

    def filter(self, filter_args, top=0, *args, **kwargs):
        headline = filter_args.get('headline')
        self.query_set = self.query_set.filter(headline__startswith=headline)
        objects = serializers.serialize(
            "json",
            self.query_set,
            fields=('headline'))

        return {
            'status': 200,
            'object': objects,
            'msg': 'sending filtered data',
        }

    def update(self, filter_args):
        # get filter value
        headline_filter = filter_args.get('headline')

        # get new value
        headline = self.request.POST.get('headline')

        print 'update start'
        print self.query_set

        # get objects
        self.query_set = self.query_set.filter(
            headline=headline_filter)
        if self.query_set.count():
            count = self.query_set.update(
                    headline=headline
                )
            return {
                'status': 200,
                "objects affected": count,
                'msg': 'update field success',
            }
        else:
            return {
                'status': 200,
                'msg': 'no object/s found',
            }

    def delete(self, filter_args):
        filters = self.resolve_filters(
            filter_args, required_fields=self.delete_filter_fields)
        instance = self.get_instance(**filters)

        if not self.has_object_perm(method='delete', obj=instance):
            raise PermissionDenied()

        self.query_set.filter(pk=instance.pk).delete()
        # Status Code 200, 204
        return {
            'status': 204,
            'msg': 'object deleted'
        }


register('polls', 'article', ArticleResource)
